  /*
1) Elektronica van https://www.amazon.nl/
Motor speed control L298N  // Fan speed control
   - Input 12V (can be between 5-35V) connected
   - 5V Power Supply for the ESP32 is provided by the L298N
Digital Temp DS18B2   // for measuring the wort temperaturethru the thremowell
SSD1306 OLED display
OLED  128 x 64 px  // http://oleddisplay.squix.ch/#/home
ESP32; AZDelivery ESP-32 
Relais module 5V Low 2 channels OT952-C102 Otronic.nl
    
 */

#include "BLEDevice.h"
#include <OneWire.h>
//#include <DallasTemperature.h>
#include <BluetoothSerial.h>
#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
//#include "font.h"

#define LED_BUILTIN 2



// ===================================== PARAMETERS ===================================================

double TEMP_DESIRED = 20.0;  // desired final temperature
double Temperature = 0.0;
double SpeedFan = 3;
double PWR_ON = 1;
int temp = 0;
int Heat = 0;
int Cool = 0;
 static char TEMP_DESIREDC[6];
  static char TemperatureC[6];
  static char SpeedFanC[6];
  static char PWR_ONC[6];
  static char HeatC[6];
  static char CoolC[6];

// ===================================== PIN CONNECTIONS ==============================================

// PIN CONNECTIONS; DS18B20 digital thermometer
// DS18B20 Board connections: red 5V (or 3.3V);  black GND;  yellow, GPIO 27 and 4.7kOhm -> 5V (or 3.3V)
//const int oneWireBus = 27;  
//OneWire oneWire(oneWireBus); // Setup a oneWire instance to communicate with any OneWire DS18B20 
//DallasTemperature sensors(&oneWire); // Pass our oneWire reference to Dallas Temperature sensor 

// PIN CONNECTIONS; SSD1306 OLED display display connected to I2C (SDA_21, SCL_22 pins); 
// connectors=GND;VCC;SCL;SDA ==> gnd; 3.3V; GPIO22; GPIO21
#define DISPLAY_1_I2C_ADDRESS 0x3C
#define DISPLAY_1_WIDTH 128 // OLED display width, in pixels
#define DISPLAY_1_HEIGHT 64 // OLED display height, in pixels
#define OLED_RESET     -1 // Reset pin # (or -1 if sharing Arduino reset pin)
TwoWire I2C_1 = TwoWire(0);

Adafruit_SSD1306 display1(DISPLAY_1_WIDTH, DISPLAY_1_HEIGHT, &I2C_1, OLED_RESET);

// PIN CONNECTIONS; Motor speed control L298N
// Fans need 12V Powersupply; connect 12V and GND to adaptor
// L298N has 6 pins starting at the 5V connection; ENA, IN1, IN2, IN3, IN4, ENB(EnableB)
// Next to 12V connector is MotorA; - and +
int fanA_IN3 = 32; // IN3
int fanA_IN4 = 33; // IN4
int fanA     = 25; // ENB


// Setting PWM properties
const int freq = 30000;
const int pwmChannel = 0;
const int resolution = 8;
const int minSpeed = 255;  // increase this if the motor does not start

//BLE server name
#define bleServerName "ESP32"

// See the following for generating UUIDs:
// https://www.uuidgenerator.net/
static BLEUUID ServiceUUID ("4fafc201-1fb5-459e-8fcc-c5c9c331914b");

// Temperature Characteristic and Descriptor
static BLEUUID TemperatureCharacteristicUUID("47524f89-07c8-43b6-bf06-a21c77bfdee8");

// TEMP_DESIRED Characteristic and Descriptor
static BLEUUID TEMP_DESIREDCharacteristicUUID("d7be7b90-2423-4d6e-926d-239bc96bb2fd");

// Fan Speed Characteristic and Descriptor
static BLEUUID SpeedFanCharacteristicUUID("f13163b4-cc7f-456b-9ea4-5c6d9cec8ee3");

// Power On Characteristic and Descriptor
static BLEUUID PWR_ONCharacteristicUUID("97f57b70-9465-4c46-a2e2-38b604f76451");

//Flags stating if should begin connecting and if the connection is up
static boolean doConnect = false;
static boolean connected = false;
static boolean doScan = false;


//Address of the peripheral device. Address will be found during scanning...
static BLEAddress *pServerAddress;

static BLEAdvertisedDevice* myDevice;

 
//Characteristics that we want to read
static BLERemoteCharacteristic* TemperatureCharacteristic;
static BLERemoteCharacteristic* TEMP_DESIREDCharacteristic;
static BLERemoteCharacteristic* SpeedFanCharacteristic;
static BLERemoteCharacteristic* PWR_ONCharacteristic;

//Activate notify
const uint8_t notificationOn[] = {0x1, 0x0};
const uint8_t notificationOff[] = {0x0, 0x0};

//Variables to store temperature and humidity
char* TemperatureChar;
char* TEMP_DESIREDChar;
char* SpeedFanChar;
char* PWR_ONChar;

//Flags to check whether new temperature and humidity readings are available
boolean newTemperature = false;
boolean newTEMP_DESIRED = false;
boolean newSpeedFan = false;
boolean newPWR_ON = false;

// Callback function that is called whenever a client is connected or disconnected
class MyClientCallback : public BLEClientCallbacks {
  void onConnect(BLEClient* pclient) {
    connected = true;
  }
void onDisconnect(BLEClient* pclient) {
    connected = false;
    Serial.println("onDisconnect");
  }
};


//Connect to the BLE Server that has the name, Service, and Characteristics
bool connectToServer(BLEAddress pAddress) {
   BLEClient* pClient = BLEDevice::createClient();
   pClient->setClientCallbacks(new MyClientCallback());

  // Connect to the remove BLE Server.
  pClient->connect(pAddress);

  // Obtain a reference to the service we are after in the remote BLE server.
  BLERemoteService* pRemoteService = pClient->getService(ServiceUUID);
  if (pRemoteService == nullptr) {
    Serial.print("Failed to find our service UUID: ");
    Serial.println(ServiceUUID.toString().c_str());
    return (false);
  }
 
  // Obtain a reference to the characteristics in the service of the remote BLE server.
  TemperatureCharacteristic = pRemoteService->getCharacteristic(TemperatureCharacteristicUUID);
  TEMP_DESIREDCharacteristic = pRemoteService->getCharacteristic(TEMP_DESIREDCharacteristicUUID);
  SpeedFanCharacteristic = pRemoteService->getCharacteristic(SpeedFanCharacteristicUUID);
  PWR_ONCharacteristic = pRemoteService->getCharacteristic(PWR_ONCharacteristicUUID);
 
  if (TemperatureCharacteristic == nullptr || TEMP_DESIREDCharacteristic == nullptr || SpeedFanCharacteristic == nullptr || PWR_ONCharacteristic == nullptr  ) {
    Serial.println("Failed to find our characteristic UUID");
    return false;
  }
 
  //Assign callback functions for the Characteristics
  TemperatureCharacteristic->registerForNotify(TemperatureNotifyCallback);
  TEMP_DESIREDCharacteristic->registerForNotify(TEMP_DESIREDNotifyCallback);
  SpeedFanCharacteristic->registerForNotify(SpeedFanNotifyCallback);
  PWR_ONCharacteristic->registerForNotify(PWR_ONNotifyCallback);
 
  return true;
}

//Callback function that gets called, when another device's advertisement has been received
class MyAdvertisedDeviceCallbacks: public BLEAdvertisedDeviceCallbacks {
  void onResult(BLEAdvertisedDevice advertisedDevice) {
    if (advertisedDevice.getName() == bleServerName) { //Check if the name of the advertiser matches
      advertisedDevice.getScan()->stop(); //Scan can be stopped, we found what we are looking for
      pServerAddress = new BLEAddress(advertisedDevice.getAddress()); //Address of advertiser is the one we need
      doConnect = true; //Set indicator, stating that we are ready to connect
    }
  }
};
 
//When the BLE Server sends a new temperature reading with the notify property
static void TemperatureNotifyCallback(BLERemoteCharacteristic* pBLERemoteCharacteristic, 
                                        uint8_t* pData, size_t length, bool isNotify) {
  
  //store temperature value
   TemperatureChar = (char*)pData;
 
  Temperature = atof(TemperatureChar);
  newTemperature = true;
 }

//When the BLE Server sends a new Temperature Desired reading with the notify property
static void TEMP_DESIREDNotifyCallback(BLERemoteCharacteristic* pBLERemoteCharacteristic, 
                                    uint8_t* qData, size_t length, bool isNotify) {
  //store Desired Temperature value
  TEMP_DESIREDChar = (char*)qData;
  newTEMP_DESIRED = true;
  TEMP_DESIRED = atof(TEMP_DESIREDChar);
 }

static void SpeedFanNotifyCallback(BLERemoteCharacteristic* pBLERemoteCharacteristic, 
                                        uint8_t* rData, size_t length, bool isNotify) {
  //store Fan Speed value
  SpeedFanChar = (char*)rData;
  SpeedFan = atof(SpeedFanChar);
  newSpeedFan = true;
}

//When the BLE Server sends a new PWR ON reading with the notify property
static void PWR_ONNotifyCallback(BLERemoteCharacteristic* pBLERemoteCharacteristic, 
                                    uint8_t* sData, size_t length, bool isNotify) {
  //store Power ON value
  PWR_ONChar = (char*)sData;
  PWR_ON = atof(PWR_ONChar);
  newPWR_ON = true; 
}

//the relays connect to
int IN1 = 4;
int IN2 = 5;
#define ON 0
#define OFF 1

void setup()
{
 relay_init();//initialize the relay

 Serial.begin(115200);
 //SSD1306_SWITCHCAPVCC= generate display voltage from 3.3V internally
 bool status1;
 I2C_1.begin();
 status1 = display1.begin(SSD1306_SWITCHCAPVCC,DISPLAY_1_I2C_ADDRESS);
 if (!(status1)){
  Serial.println(F("SSD1306 INITIALIZATION failed"));
  for(;;);
  }
 display1.clearDisplay();
  display1.setTextSize(2);
  display1.setTextColor(WHITE,0);
  display1.setCursor(0,25);
  display1.print("Fermenter Client");
  display1.display();

 //init BLE device
 BLEDevice::init("");

 BLEScan* pBLEScan = BLEDevice::getScan();
  pBLEScan->setAdvertisedDeviceCallbacks(new MyAdvertisedDeviceCallbacks());
  pBLEScan->setActiveScan(true);
  pBLEScan->start(30);


//Fan setup
pinMode(fanA_IN3,OUTPUT);
pinMode(fanA_IN4,OUTPUT);
pinMode(fanA,OUTPUT);
ledcSetup(pwmChannel,freq,resolution); //configure LED PWM functionalities
ledcAttachPin(fanA,pwmChannel); //attach the channel to the GPIO to be controlled
digitalWrite(fanA_IN3,HIGH);
digitalWrite(fanA_IN4,LOW);
ledcWrite(pwmChannel,255);
delay(1000);
if (Temperature == 0.0) {
  (Temperature = TEMP_DESIRED);
}
   if ((Temperature - TEMP_DESIRED) > 0.0 &&(Temperature - TEMP_DESIRED) < 0.5 ) {
    (temp = 0);
    (Heat = 1);
     relay_SetStatus(ON, OFF);//turn on RELAY_1 Heat
    }
   if ((TEMP_DESIRED - Temperature) > 0.0 && (TEMP_DESIRED - Temperature) < 0.5) {
    (temp = 2);
    (Cool = 1);
     relay_SetStatus(OFF, ON);//turn on RELAY_2 Cool
    }
}

void relay_init(void)//initialize the relay
{
 //set all the relays OUTPUT
 pinMode(IN1, OUTPUT);
 pinMode(IN2, OUTPUT);
 relay_SetStatus(OFF, OFF); //turn off all the relay
}
//set the status of relays
void relay_SetStatus( unsigned char status_1, unsigned char status_2)
{
 digitalWrite(IN1, status_1);
 digitalWrite(IN2, status_2);
}
void FanSymbol(int SpeedFan)
{ if   (newSpeedFan = true){

  display1.setTextSize(1); 
  if (SpeedFan >= 1.00){
  display1.setCursor(92,15); display1.print((char)178 );}
  display1.setTextSize(2); 
  if (SpeedFan >= 2.00){
  display1.setCursor(96,7); display1.print((char)178 );}
  display1.setTextSize(3); 
  if (SpeedFan >= 3.00){
  display1.setCursor(100,0); display1.print((char)178 );}
}
}
void Revs (int SpeedFan)
{  if (newSpeedFan = true) {

   if (SpeedFan == 3.00){
    ledcWrite(pwmChannel,255);
   }
  if (SpeedFan == 2.00){
    ledcWrite(pwmChannel,175);
   }
  if (SpeedFan == 1.00){
    ledcWrite(pwmChannel,100);
   }
     newSpeedFan = false;
 }
}


String FloatToStr(float f,int d){
  int decimal=round((f - int(f))*pow(10,d));
  return String(int(f)) + "." + String(decimal);
}



void loop() {
if (doConnect == true) {
       if (connectToServer(*pServerAddress)) {
      Serial.println("We are now connected to the BLE Server.");
      //Activate the Notify property of each Characteristic
    TemperatureCharacteristic->getDescriptor(BLEUUID((uint16_t)0x2902))->writeValue((uint8_t*)notificationOn, 2, true);
    TEMP_DESIREDCharacteristic->getDescriptor(BLEUUID((uint16_t)0x2902))->writeValue((uint8_t*)notificationOn, 2, true);
    SpeedFanCharacteristic->getDescriptor(BLEUUID((uint16_t)0x2902))->writeValue((uint8_t*)notificationOn, 2, true);
    PWR_ONCharacteristic->getDescriptor(BLEUUID((uint16_t)0x2902))->writeValue((uint8_t*)notificationOn, 2, true);
    connected = true;
 //   } else {
 //     Serial.println("We have failed to connect to the server; Restart your device to scan for nearby BLE server again.");
    }
       doConnect = false;
}


 //Digital temperature DS18B20
 //sensors.requestTemperatures();
 //float temperature = sensors.getTempCByIndex(0);
  //delay(1000);
if (PWR_ON == 0.00){
  relay_SetStatus(OFF, OFF);//turn off RELAYS
  ledcWrite(pwmChannel,0);
  display1.clearDisplay();
  display1.setTextColor(WHITE);
  display1.setTextSize(1); 
  display1.setCursor(0,0); display1.print("off");
  display1.setTextSize(1); 
  display1.setCursor(27,13);display1.print("---");
  FanSymbol(SpeedFan);
  display1.setTextSize(1); 
  display1.setCursor(0,18); display1.print("____________________");
  display1.setTextSize(2);
  display1.setCursor(0,30); display1.print("Set:");
  display1.setCursor(50,30);display1.print(FloatToStr(TEMP_DESIRED,1)+(char)247 + "C");
  display1.setCursor(0,50); display1.print("Act:");
  display1.setCursor(50,50); display1.print(FloatToStr(Temperature,1)+(char)247 + "C");
  display1.display();
  delay(500);

} else {

 if (TEMP_DESIRED == Temperature) {
  Heat = 0;
  Cool = 0;
  relay_SetStatus(OFF, OFF);//turn off RELAY_1 and RELAY_2
  ledcWrite(pwmChannel,100);
  display1.clearDisplay();
  display1.setTextColor(WHITE);
  display1.setTextSize(1); 
  display1.setCursor(0,0); display1.print("on");
  display1.setTextSize(1); 
  display1.setCursor(27,13);display1.print("---");
  display1.setTextSize(1); 
  display1.setCursor(0,18); display1.print("____________________");
  display1.setTextSize(1); 
  display1.setCursor(70,0); display1.print("o");
  display1.setCursor(78,0); display1.print("o");
  display1.setCursor(70,8);display1.print("o");
  display1.setCursor(78,8);display1.print("o");
  display1.setCursor(74,4);display1.print("/");
  display1.setCursor(76,4);display1.print((char)91);
  FanSymbol(SpeedFan);
  display1.setTextSize(2);
  display1.setCursor(0,30); display1.print("Set:");
  display1.setCursor(50,30);display1.print(FloatToStr(TEMP_DESIRED,1)+(char)247 + "C");
   display1.setCursor(0,50); display1.print("Act:");
  display1.setCursor(50,50); display1.print(FloatToStr(Temperature,1)+(char)247 + "C");
   display1.display();
  temp = 1;
  delay(500);
 }
    
 if ((Temperature - TEMP_DESIRED) > 0.5) {
 Heat = 0;
 Cool = 1;
 relay_SetStatus(OFF, ON);//turn on RELAY_2 Cool
 Revs(SpeedFan);
 display1.clearDisplay();
 display1.setTextColor(WHITE);
 display1.setTextSize(1); 
 display1.setCursor(0,0); display1.print("on");
 display1.setTextSize(3); 
 display1.setCursor(25,0); display1.print("*");
 display1.setTextSize(1); 
 display1.setCursor(70,0); display1.print("o");
 display1.setCursor(78,0); display1.print("o");
 display1.setCursor(70,8);display1.print("o");
 display1.setCursor(78,8);display1.print("o");
 display1.setCursor(74,4);display1.print("/");
 display1.setCursor(76,4);display1.print((char)91);
 FanSymbol(SpeedFan);
 display1.setTextSize(1); 
 display1.setCursor(0,18); display1.print("____________________");
 display1.setTextSize(2);
 display1.setCursor(0,30); display1.print("Set:");
 display1.setCursor(50,30);display1.print(FloatToStr(TEMP_DESIRED,1)+(char)247 + "C");
 display1.setCursor(0,50); display1.print("Act:");
 display1.setCursor(50,50); display1.print(FloatToStr(Temperature,1)+(char)247 + "C");
 display1.display();
 temp = 0;
 delay(500);
 }
  if ((Temperature - TEMP_DESIRED) > 0.0 &&(Temperature - TEMP_DESIRED) < 0.5 ) {
 if (temp == 0){
Heat = 0;
Cool = 1;
display1.clearDisplay();
 display1.setTextColor(WHITE);
 display1.setTextSize(1); 
 display1.setCursor(0,0); display1.print("on");
 display1.setTextSize(3); 
 display1.setCursor(25,0); display1.print("*");
 display1.setTextSize(1); 
 display1.setCursor(70,0); display1.print("o");
 display1.setCursor(78,0); display1.print("o");
 display1.setCursor(70,8);display1.print("o");
 display1.setCursor(78,8);display1.print("o");
 display1.setCursor(74,4);display1.print("/");
 display1.setCursor(76,4);display1.print((char)91);
 FanSymbol(SpeedFan);
 display1.setTextSize(1); 
 display1.setCursor(0,18); display1.print("____________________");
 display1.setTextSize(2);
 display1.setCursor(0,30); display1.print("Set:");
  display1.setCursor(50,30);display1.print(FloatToStr(TEMP_DESIRED,1)+(char)247 + "C");
  display1.setCursor(0,50); display1.print("Act:");
  display1.setCursor(50,50); display1.print(FloatToStr(Temperature,1)+(char)247 + "C");
   display1.display();
 delay(500);
  } else {
  Heat = 0;
  Cool = 0;
  display1.clearDisplay();
  display1.setTextColor(WHITE);
  display1.setTextSize(1); 
  display1.setCursor(0,0); display1.print("on");
  display1.setTextSize(1); 
  display1.setCursor(27,13);display1.print("---");
  display1.setTextSize(1); 
  display1.setCursor(0,18); display1.print("____________________");
  display1.setTextSize(1); 
  display1.setCursor(70,0); display1.print("o");
  display1.setCursor(78,0); display1.print("o");
  display1.setCursor(70,8);display1.print("o");
  display1.setCursor(78,8);display1.print("o");
  display1.setCursor(74,4);display1.print("/");
  display1.setCursor(76,4);display1.print((char)91);
  FanSymbol(SpeedFan);
  display1.setTextSize(2);
  display1.setCursor(0,30); display1.print("Set:");
   display1.setCursor(50,30);display1.print(FloatToStr(TEMP_DESIRED,1)+(char)247 + "C");
   display1.setCursor(0,50); display1.print("Act:");
  display1.setCursor(50,50); display1.print(FloatToStr(Temperature,1)+(char)247 + "C");
   display1.display();
   delay(500);
  }
}
 if ((TEMP_DESIRED - Temperature) > 0.5) {
Heat = 1;
Cool = 0;
relay_SetStatus(ON, OFF);//turn on RELAY_1 Heat
 Revs(SpeedFan);
 display1.clearDisplay();
 display1.setTextColor(WHITE);
 display1.setTextSize(1); 
 display1.setCursor(0,0); display1.print("on");
 display1.setCursor(27,0); display1.print("$$$");
 display1.setCursor(25,7); display1.print("$$$");
 display1.setCursor(27,13);display1.print("$$$");
 display1.setTextSize(1); 
 display1.setCursor(70,0); display1.print("o");
 display1.setCursor(78,0); display1.print("o");
 display1.setCursor(70,8);display1.print("o");
 display1.setCursor(78,8);display1.print("o");
 display1.setCursor(74,4);display1.print("/");
 display1.setCursor(76,4);display1.print((char)91);
 FanSymbol(SpeedFan);
 display1.setTextSize(1); 
 display1.setCursor(0,18); display1.print("____________________");
 display1.setTextSize(2);
 display1.setCursor(0,30); display1.print("Set:");
  display1.setCursor(50,30);display1.print(FloatToStr(TEMP_DESIRED,1)+(char)247 + "C");
  display1.setCursor(0,50); display1.print("Act:");
  display1.setCursor(50,50); display1.print(FloatToStr(Temperature,1)+(char)247 + "C");
 display1.display();
 temp = 2;
 delay(500);
 }
  if ((TEMP_DESIRED - Temperature) > 0.0 && (TEMP_DESIRED - Temperature) < 0.5) {
 if (temp == 2) {
 Heat = 1;
 Cool = 0;
 display1.clearDisplay();
 display1.setTextColor(WHITE);
 display1.setTextSize(1); 
 display1.setCursor(0,0); display1.print("on");
 display1.setCursor(27,0); display1.print("$$$");
 display1.setCursor(25,7); display1.print("$$$");
 display1.setCursor(27,13);display1.print("$$$");
 display1.setTextSize(1); 
 display1.setCursor(70,0); display1.print("o");
 display1.setCursor(78,0); display1.print("o");
 display1.setCursor(70,8);display1.print("o");
 display1.setCursor(78,8);display1.print("o");
 display1.setCursor(74,4);display1.print("/");
 display1.setCursor(76,4);display1.print((char)91);
 FanSymbol(SpeedFan);
 display1.setTextSize(1); 
 display1.setCursor(0,18); display1.print("____________________");
 display1.setTextSize(2);
 display1.setCursor(0,30); display1.print("Set:");
  display1.setCursor(50,30);display1.print(FloatToStr(TEMP_DESIRED,1)+(char)247 + "C");
   display1.setCursor(0,50); display1.print("Act:");
  display1.setCursor(50,50); display1.print(FloatToStr(Temperature,1)+(char)247 + "C");
  display1.display();
 delay(500);
   } else {
  Heat = 0;
  Cool = 0;
  display1.clearDisplay();
  display1.setTextColor(WHITE);
  display1.setTextSize(1); 
  display1.setCursor(0,0); display1.print("on");
  display1.setTextSize(1); 
  display1.setCursor(27,13);display1.print("---");
  display1.setTextSize(1); 
  display1.setCursor(0,18); display1.print("____________________");
  display1.setTextSize(1); 
  display1.setCursor(70,0); display1.print("o");
  display1.setCursor(78,0); display1.print("o");
  display1.setCursor(70,8);display1.print("o");
  display1.setCursor(78,8);display1.print("o");
  display1.setCursor(74,4);display1.print("/");
  display1.setCursor(76,4);display1.print((char)91);
  FanSymbol(SpeedFan);
  display1.setTextSize(2);
  display1.setCursor(0,30); display1.print("Set:");
   display1.setCursor(50,30);display1.print(FloatToStr(TEMP_DESIRED,1)+(char)247 + "C");
  display1.setCursor(0,50); display1.print("Act:");
  display1.setCursor(50,50); display1.print(FloatToStr(Temperature,1)+(char)247 + "C");
   display1.display();
  delay(500);
    }
  }

     // If we are connected to a peer BLE Server, update the characteristic each time we are reached
  // with the current time since boot.
  if (connected) {
    // do something fun here

  }else if(doScan){
    BLEDevice::getScan()->start(1);  // this is just example to start scan after disconnect, most likely there is better way to do it in arduino
  }

  // In this example "delay" is used to delay with one second. This is of course a very basic 
  // implementation to keep things simple. I recommend to use millis() for any production code

}
}
